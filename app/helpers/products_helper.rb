module ProductsHelper
  # fixing the price display
  def print_price(price)
    number_to_currency price
  end
  
  # If we don’t send in a value for requested, and thus have only one parameter, 
  # it’ll just set requested to one
  def print_stock(stock, requested = 1)
    if stock == 0
      content_tag(:span, "Out of stock", class: "in_stock")
    elsif stock >= requested
      content_tag(:span, "In stock (#{stock})", class: "in_stock")
    else
      content_tag(:span, "Inefficient stock (#{stock})", class: "low_stock")
    end
  end
end
