class Order < ActiveRecord::Base
  has_many :order_items, dependent: :destroy
  belongs_to :user
  belongs_to :address
  
  def total
    order_items.map(&:subtotal).sum
    # alternatively
    # order_items.to_a.sum { |item| item.subtotal }
  end
end
