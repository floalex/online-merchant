class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # helper_method line makes the method available to all our views
  helper_method :current_user
  
  private
  
  # Even though the current_user method is private in ApplicationController, 
  # it will be available to all our controllers because they inherit from ApplicationController
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
  def load_order
    @order = Order.find_or_initialize_by(id: session[:order_id],
                                            user_id: session[:user_id], status: "unsubmitted")
    if @order.new_record?
      @order.save!
      session[:order_id] = @order.id
    end
  end
end
