== README

Jumpstart 2nd level Rails project: An e-commerce site for a small grocery


* OmniAuth--The minimum expectations for an OmniAuth provider are three things:

1.provider – A string name uniquely identifying the provider service
2.uid – An identifying string uniquely identifying the user within that provider
3.name – Some kind of human-meaningful name for the user

* This app uses Rails dynamic finders:

1.find_or_create: Attempt to find a match in the database based on the following parameters. 
If found, return it. Otherwise, initialize a new record with these values and save it.
2._by_provider_and_uid_and_name: Do the lookup and initialization using the provider 
(first parameter), the uid (second parameter), and the name (third).